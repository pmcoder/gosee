package gosee

import(
    "net/http"
    "fmt"
    "encoding/json"
    "bytes"
    "strconv"
    "log"
)

const mirrorURIPattern = "https://www.googleapis.com/mirror/v1/%s"
//methods
const m_POST = "POST"
const m_GET = "GET"
const m_DELETE = "DELETE"
const m_PUT = "PUT"
const m_PATCH = "PATCH"

//service endpoints
const s_CONTACTS = "contacts"
const s_TIMELINE = "timeline"
const s_LOCATIONS = "locations"

var mirrorService *MirrorService

type MirrorService struct {
    
}

func NewMirrorService() *MirrorService{
    if mirrorService == nil{
        mirrorService = &MirrorService{}
    }
    return mirrorService
}

type Mirror struct {
    service *MirrorService
}

func NewMirror(ms *MirrorService) *Mirror{
    return &Mirror{service:ms}    
}

//Send the request to Mirror API
func (ms *MirrorService) sendRequest(authToken, serviceEndpoint string, data interface{}, id *string, transMeth string) (err error){
    
    //marshal sturct to json
    d, err := json.Marshal(data)
    
    if err != nil {
        return
    }
    
    buf := bytes.NewBuffer(d)
    
    client := &http.Client{}
    var req *http.Request
    
    uri := fmt.Sprintf(mirrorURIPattern, serviceEndpoint)
    //if put, patch , get or delete we may need to append the id
    if id != nil {
        uri = uri + "/"+ *id   
    }
        
    if transMeth == m_GET || transMeth == m_DELETE {
        req, err = http.NewRequest(transMeth, uri, nil)  
    }
    
    if transMeth == m_POST || transMeth == m_PATCH || transMeth == m_PUT {
        uri := fmt.Sprintf(mirrorURIPattern, serviceEndpoint)
        req, err = http.NewRequest(transMeth, uri, buf) 
        req.Header.Add("Host", "www.googleapis.com")
        req.Header.Add("Content-Type", "application/json")
        req.Header.Add("Content-Length", strconv.Itoa(len(d)))
    }
    
    
    req.Header.Add("Authorization", "Bearer "+ authToken)
    
    resp, err := client.Do(req)
    
    if err != nil && resp.StatusCode != 200 {
        //make new error with status included
    }
    log.Printf("Mirror API call successful with status: %d url: %s %s", resp.StatusCode, uri, req)
    return
}

/*
* Timeline functions
*/
func (m *Mirror) InsertTimelineItem(authToken string, ti *TimelineItem) error{
    return m.service.sendRequest(authToken, s_TIMELINE, *ti, nil, m_POST)
}

/*
* Subscription functions
*/

/*
* Location functions
*/
func(m *Mirror) GetLocation(authToken, id string) (*Location) error {
    return m.service.sendRequest(authToken, s_LOCATIONS, nil, id, m_GET)   
}

func(m *Mirror) GetLocatoinList(authToken string) (*[]Location error) {
    return m.service.sendRequest(authToken, s_LOCATIONS, nil, nil, m_GET)   
}

func(m *Mirror) GetLatestLocation(authToken string) (*Location) error {
    return m.service.sendRequest(authToken, s_LOCATIONS, nil, "latest", m_GET)   
}


/*
* Contact functions
*/
func(m *Mirror) InsertContact(authToken string, c *Contact) error {
    return m.service.sendRequest(authToken, s_CONTACTS, c, nil, m_POST)   
}

func(m *Mirror) GetContact(authToken string, id string) error {
    return m.service.sendRequest(authToken, s_CONTACTS, nil, &id, m_GET)   
}

func(m *Mirror) DeleteContact(authToken string, id string) error {
    return m.service.sendRequest(authToken, s_CONTACTS, nil, &id, m_DELETE)   
}

func(m *Mirror) ListContacts(authToken string) error {
    return m.service.sendRequest(authToken, s_CONTACTS, nil, nil, m_GET)   
}

func(m *Mirror) UpdateContact(authToken, id string, c *Contact) error {
    return m.service.sendRequest(authToken, s_CONTACTS, c, &id, m_PUT)   
}