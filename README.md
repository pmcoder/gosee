GoSee is an alternative implementation of the Google Glass Mirror API in Go.

This is NOT a complete implementation (maybe not even a good one) and methods are added as needed while exploring Glass. Don't plan for any production use out of this if you decide to check it out.

Still in early stage work and probably not functional much of the time.

Data model structs shamelessly ripped from the Google implementation. 
https://code.google.com/p/google-api-go-client/source/browse/mirror/v1/mirror-gen.go